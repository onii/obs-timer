#!/bin/bash
date1=`date +%s`

echo "Press q to quit."
while [ true ]; do 
   read -t 1 -n 1 tmp_var
   echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)" \
    > timer.txt
   if [ ! -z $tmp_var ]; then
       echo "00:00:00" > timer.txt
       exit 0
   fi
done
